/* Converter for 24bit/96kHz audio files to 16bit/48kHz. */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <math.h>

#include <samplerate.h>
#include <sndfile.h>

#define BUFFER_LEN 65536


void usage() {
  fprintf(stderr,
          "resample - A sample size / rate converter.\n"
          "Usage: resample [<OPTIONS>] <INPUT_FILE>\n"
          "\n"
          "Options:\n"
          "\n"
          "  -o PATH      Write the output file to PATH (required)\n"
          "  -b BITS      Output bits per sample (default: 16)\n"
          "  -r RATE      Set output sample rate (default: 48000)\n"
          "  -c N         Select a SRC converter\n"
          "\n"
          "SRC converters:\n"
          "\n"
          "  0 : Best Sinc Interpolator (default)\n"
          "  1 : Medium Sinc Interpolator\n"
          "  2 : Fastest Sinc Interpolator\n"
          "  3 : ZOH Interpolator\n"
          "  4 : Linear Interpolator\n"
          "\n");
  exit(1);
}

static double apply_gain(float *data, long frames, int channels, double max, double gain)
{
  long k;

  for (k = 0 ; k < frames * channels ; k++) {
    data[k] *= gain;
    if (fabs(data[k]) > max)
      max = fabs(data[k]) ;
  }
  return max;
}

static sf_count_t convert(SNDFILE *input_file, int input_bits,
                          SNDFILE *output_file, int output_bits,
                          int converter, double ratio, int channels,
                          double *gain) {
  static float input[BUFFER_LEN];
  static float output[BUFFER_LEN];

  SRC_STATE *src_state;
  SRC_DATA src_data;
  int error;
  double max = 0;
  sf_count_t output_count = 0;

  sf_seek(input_file, 0, SEEK_SET);
  sf_seek(output_file, 0, SEEK_SET);

  /* Initialize SRC */
  src_state = src_new(converter, channels, &error);
  if (!src_state) {
    fprintf(stderr, "src_new() failed\n");
    exit(2);
  }

  src_data.end_of_input = 0;
  src_data.input_frames = 0;
  src_data.data_in = input;
  src_data.src_ratio = ratio;
  src_data.data_out = output;
  src_data.output_frames = BUFFER_LEN / channels;

  while (1) {
    int n = BUFFER_LEN / channels;
    if (src_data.input_frames == 0) {
      src_data.input_frames = sf_readf_float(input_file, input, n);
      src_data.data_in = input;
      if (src_data.input_frames < n)
        src_data.end_of_input = SF_TRUE;
    }

    error = src_process(src_state, &src_data);
    if (error) {
      fprintf(stderr, "SRC Error: %s\n", src_strerror(error));
      exit(2);
    }

    if (src_data.end_of_input && src_data.output_frames_gen == 0)
      break;

    max = apply_gain(src_data.data_out, src_data.output_frames_gen, channels, max, *gain);

    sf_writef_float(output_file, output, src_data.output_frames_gen);
    output_count += src_data.output_frames_gen;

    src_data.data_in += src_data.input_frames_used * channels;
    src_data.input_frames -= src_data.input_frames_used;
  }

  src_state = src_delete(src_state);
  if (max > 1.0) {
    *gain = 1.0 / max;
    fprintf(stderr, "Output has clipped, restarting conversion with gain=%g\n", *gain);
    return -1;
  }

  return output_count;
}

static void resample(char *input_file_name, char *output_file_name,
                     int output_rate, int output_bits, int converter) {
  SNDFILE *input_file, *output_file;
  SF_INFO sfinfo;
  int input_bits;
  double ratio;
  double gain = 1.0;
  sf_count_t count;

  if (src_get_name(converter) == NULL) {
    fprintf(stderr, "Unknown SRC converter\n");
    exit(1);
  }

  input_file = sf_open(input_file_name, SFM_READ, &sfinfo);
  if (input_file == NULL) {
    fprintf(stderr, "Unable to open input file \"%s\"\n", input_file_name);
    exit(1);
  }

  /* Decode the sample format and check what to do. */
  switch (sfinfo.format & SF_FORMAT_SUBMASK) {
    case SF_FORMAT_PCM_16:
      input_bits = 16;
      break;
    case SF_FORMAT_PCM_24:
      input_bits = 24;
      break;
    case SF_FORMAT_PCM_32:
      input_bits = 32;
      break;
    default:
      fprintf(stderr, "Unsupported input sample format\n");
      exit(1);
  }

  printf("Input file: %s\n", input_file_name);
  printf("Input sample rate: %d\n", sfinfo.samplerate);
  printf("Input sample size: %d\n", input_bits);

  printf("Output file: %s\n", output_file_name);
  printf("Output sample rate: %d\n", output_rate);
  printf("Output sample size: %d\n", output_bits);

  /* Compute the sample rate ratio and do a sanity check of the parameters */
  ratio = (double)output_rate / sfinfo.samplerate;
  if (!src_is_valid_ratio(ratio)) {
    fprintf(stderr, "Invalid sample rate conversion ratio %g\n", ratio);
    exit(1);
  }

  if (fabs(ratio - 1.0) < 1e-20) {
    fprintf(stderr, "Sample rates are the same\n");
    exit(1);
  }

  /* Set the desired output parameters. */
  {
    int output_format = sfinfo.format & (SF_FORMAT_TYPEMASK | SF_FORMAT_ENDMASK);
    sfinfo.samplerate = output_rate;
    switch (output_bits) {
      case 16:
        sfinfo.format = output_format | SF_FORMAT_PCM_16;
        break;
      case 24:
        sfinfo.format = output_format | SF_FORMAT_PCM_24;
        break;
      case 32:
        sfinfo.format = output_format | SF_FORMAT_PCM_32;
        break;
    }
  }

  if (!sf_format_check(&sfinfo)) {
    fprintf(stderr, "Output parameters are invalid!\n");
    exit(2);
  }

  /* Attempt to perform the sample rate conversion. The loop might run
   * multiple times with different 'gain' parameters, until the
   * conversion can be performed without clipping.
   */
  do {
    output_file = sf_open(output_file_name, SFM_WRITE, &sfinfo);
    if (!output_file) {
      fprintf(stderr, "Could not open output file \"%s\"\n", output_file_name);
      sf_close(input_file);
      exit(1);
    }

    sf_command(output_file, SFC_SET_UPDATE_HEADER_AUTO, NULL, SF_TRUE);
    count = convert(input_file, input_bits, output_file, output_bits,
                    converter, ratio, sfinfo.channels, &gain);
    sf_close(output_file);
  } while (count < 0);

  sf_close(input_file);
}

int main(int argc, char **argv) {
  int c;
  int output_rate = 48000;
  int output_bits = 16;
  int converter = SRC_SINC_MEDIUM_QUALITY;
  char *output_file_name = NULL;

  while ((c = getopt(argc, argv, "hb:c:o:r:")) != EOF) {
    switch (c) {
      case 'h':
        usage();
      case 'b':
        output_bits = atoi(optarg);
        break;
      case 'c':
        converter = atoi(optarg);
        break;
      case 'o':
        output_file_name = optarg;
        break;
      case 'r':
        output_rate = atoi(optarg);
        if (output_rate < 1000) {
          output_rate *= 1000;
        }
        break;
    }
  }

  if (argc - optind != 1) {
    fprintf(stderr, "Wrong number of arguments\n");
    exit(1);
  }

  if (!output_file_name) {
    fprintf(stderr, "Must specify an output file with -o\n");
    exit(1);
  }

  resample(argv[optind], output_file_name, output_rate, output_bits, converter);
}
